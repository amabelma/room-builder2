var uiConfig = {
    signInSuccessUrl: window.location,
    // Terms of service url.
    signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
    tosUrl: ''
};

var ui = new firebaseui.auth.AuthUI(firebase.auth());
ui.start('#firebaseui-auth-container', uiConfig);

document.getElementById('sign-out').addEventListener('click', function(event) {
    firebase.auth().signOut();
});

initApp = function() {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            document.getElementById('firebaseui-auth-container').style.display ='none';
            document.getElementById('canvas').style.display ='block';
        } else {
            // User is signed out.
            document.getElementById('sign-out').style.display = 'none';
            document.getElementById('firebaseui-auth-container').style.display ='inline';
            document.getElementById('canvas').style.display ='none';
        }
    }, function(error) {
        console.log(error);
    });
};

window.addEventListener('load', function() {
    initApp()
});