var boxWidth = 800;
var boxHeight = 600;
var padding = 10;

var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

function drawBoard(){
    for (var index = 0; index <= boxWidth; index += 40) {
        context.moveTo(0.5 + index + padding, padding);
        context.lineTo(0.5 + index + padding, boxHeight + padding);
    }

    for (var index = 0; index <= boxHeight; index += 40) {
        context.moveTo(padding, 0.5 + index + padding);
        context.lineTo(boxWidth + padding, 0.5 + index + padding);
    }

    context.strokeStyle = "grey";
    context.stroke();
}

drawBoard();