(
    ()=> {
        function stateManager() {
            let _this = {};

            let state = {};

            _this.setState = function (key, value) {
                state[key] = value;
            }

            _this.getState = function (key) {
                return state[key];
            }

            return _this;
        }
        window.globalState = stateManager();
    }
)()